This directory contains the Orocos2 Simulink Toolbox

This toolbox can be used to generate OROCOS2 component from Simulink models using MATLAB Code Generator. It contains Simulink block library and code template files. The project is based on OROCOS Simulink Toolbox (http://www.orocos.org/orocos/orocos-simulink-toolbox), which works with OROCOS 1.

INSTALLATION

Copy the contents of this directory ('orocos/*') into your
'MATLAB_ROOT/rtw/c/orocos' directory.

You should then have the following layout:

 MATLAB_ROOT
 `-rtw
   `-c
     `-orocos
        |`orocos.tlc
        |`...
        `-taskcontext
            |`orocos2_taskcontext.slx
            |`...
            `-slbocks.m


Next, from within Matlab, run 'cd rtw/c/orocos; install.m' script, which will
configure your mex compiler and generate the files needed by
Simulink. Once this is done, An Orocos2forSimulink has appeared in your
library browser and you can start creating Orocos models and generate
Orocos components.

In case you need to set the paths again, you can use 'setpath.m' which
only adds the required paths to your global paths environment.

USAGE

1. Create your model. Block properties are self-explanatory, port name coincides with block name.

2. Invoke Code Builder. Select target `orocos.tlc', and C language. Also you can build sources generated 
   by MATLAB using command like this:
    
       make -f <MODEL>.mk install MATLAB_ROOT=/opt/MATLAB/R2014a OROPATH=/opt/ros/indigo INSTALL_PREFIX=~/ros_ws/devel \
             MODELLIB=mullib.a RELATIVE_PATH_TO_ANCHOR=.. MODELREF_TARGET_TYPE=NONE OPTS="-DRT_MALLOC"

3. Add following make variables if necessary:
	MODEL_NAME --- component name (default value is Simulink model name).
	PACKAGE --- package name, default value is `RTW'.
	OROCOS_TARGET --- target platform, default value is `gnulinux'.
	INSTALL_PREFIX --- compiled module (*.so file) will be placed under $(INSTALL_PREFIX)/lib/orocos/$(OROCOS_TARGET)/$(PACKAGE)/ 
        If you use ROS with catkin make system set INSTALL_PREFIX to your development or installation space.
	MATLAB_ROOT --- directory where MATLAB is located, `/usr/local/matlab' is default value.
	OROPATH --- directory where OROCOS is located, `/usr/local' is default value.
    CCACHE --- ccache executable, it dramatically reduce recompilation time. By default Makefile tries to guess if it presents on your system

4. Click `Build' button.

5. Run component. In OROCOS deployer execute following commands:

    import("<PACKAGE>")
    loadComponent("model", "RTW::<MODEL_NAME>")
	model.setPeriod(<BASERATE>)
	model.configure()
    model.start()

Note that after assigment of base rate you always must call `configure()'.

RUNTIME COMPONENT BEHAVOUR:

Depending on component Activity period there are following modes:

  a) If period is equal to Simulink fundamental sample time the component runs 
     in periodic mode. Each period fundamental model step is performed.

  b) If period is positive but is not equal to Simulink fundamental sample time then
     the latter is modified. This operation may cause unwanted behavour, 
     so Warning is issued.

  c) If period is equal to zero then model fundamental steps are triggered by New 
     Data Event on event InputPorts. If model does not contains any Event Ports, 
     base simulation loop will not be executed. In this mode the model will be working 
     correcty only if interval beetween New Data Events is equal to Simulink model 
     fundamental rate.

TIPS:

* Recompiling project with MATLAB may take long time. Code Generator recreates 
  Makefile each time you press `Build' button, so it is always newer than object 
  files and make utility does not skip compilation of existing object files. To 
  avoid this problem after first compilation remove checkbox `Generate makefile' 
  and compile generated code invoking make comman from MATLAB building log.

* Another way to increase compilation speed is to install ccache utility
  (https://ccache.samba.org/). Makefile should be able to detect its presence 
  automaticaly.

NOTES:

* We only support single threaded models.
  This is because an Orocos component can execute only one thread.
  In case you need multiple threads, create multiple models and
  generate a component from each model. Multiple rates are supported.

* In some Matlab versions we have issues with wrong code generation.
  It is yet unclear if we can influence it with a flag in
  orocos.tlc... Typically, the compiler complains about a statement like
  (in the file 'test1_orocos/test1.c' from model 'test1.m'):
  
    Parameters_test1 *((Parameters_test1 *) test1_rtM->ModelData.defaultParam) =
      (Parameters_test1 *) test1_rtM->ModelData.defaultParam;

  The solution is to remove these two lines. This will not change the
  behaviour of your model. You need to do this each time code is
  generated.

* Extended mode support is currently not enabled in orocos.tlc.  
  Because extended mode is supported under Linux by Simulink, it
  should not be hard to enable it and set the correct options in
  orocos.tlc. See Matlab's grt.tlc for an example of setting the external 
  mode options. The RTWComponent class has the proper hooks in place
  to support extended mode, but this has not yet been tested.