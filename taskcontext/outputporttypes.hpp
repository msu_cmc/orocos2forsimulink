#ifndef  OUTPUTPORTTYPES_HPP
#define  OUTPUTPORTTYPES_HPP

#include <string>
#include <rtt/OutputPort.hpp>
#include <rtt/TaskContext.hpp>

#include "orocosservice.hpp"

/**
 * @brief Orocos OutputPort abstract class.
 * Create port of specified type and add it to TaskContext.
 **/
template <typename T> class OrocosOutputPort : public OrocosService, public OrocosOutputPortInterface
{
	protected:
		RTT::OutputPort< T > * port;

	public:
		OrocosOutputPort(RTT::TaskContext * _tc, const std::string& _name, const std::string& _desc) 
			: OrocosService(_tc, _name, _desc) 
		{
			if ( tc->ports()->getPort( name ) == 0 ) {
				//create new Port and add it to TaskContext
				port = new RTT::OutputPort< T >(name);
				port->doc(desc);
				cleanup = true; 
				tc->ports()->addPort( *port );
			} 
			else {
				//get pointer 
				port = tc->ports()->getPortType< RTT::OutputPort< T > >(name);
				cleanup = false;
				if ( !port ) {
					std::string err_msg;
					err_msg = "OutputPort " + name + " has wrong type in TaskContext.";
					throw OrocosError(err_msg.c_str());
				}
			}
		}

		~OrocosOutputPort()
		{
			if (cleanup) {
				tc->ports()->removePort(name);
				delete port;
			}
		}
};


/**
 * @brief OutputPort envelop for scalar numerical types.
 * OutputPort envelop for scalar numerical types.
 **/
template <typename T> class OrocosScalarOutputPort : public OrocosOutputPort< T >
{
	public:
		OrocosScalarOutputPort(RTT::TaskContext * _tc, const std::string& _name, const std::string& _desc) 
			: OrocosOutputPort< T >(_tc, _name, _desc) 
		{ }

		int getDimention() { return 1; }

		void write(const real_T * s_in_port)
		{
			T data = *s_in_port;
			this->port->write( data );
		}
};

/**
 * @brief OutputPort envelop for vector numerical types.
 * OutputPort envelop for vector numerical types.
 **/
template <typename T> class OrocosVectorOutputPort : public OrocosOutputPort< std::vector<T> >
{
	protected:
		std::vector< T > val;

	public:
		OrocosVectorOutputPort(RTT::TaskContext * _tc, const std::string& _name, const std::string& _desc, unsigned int width) 
			: OrocosOutputPort< std::vector<T> >(_tc, _name, _desc)
		{
			val.resize(width);
			this->port->setDataSample(val);
		}

		int getDimention() { return val.size(); }

		void write(const real_T * s_in_port)
		{
			for(unsigned int i = 0; i < val.size(); i++) val[i] = s_in_port[i];
			this->port->write(val);
		}
};
#endif  /*OUTPUTPORTTYPES_HPP*/
