#ifndef  PROPERTYTYPES_HPP
#define  PROPERTYTYPES_HPP

#include <string>
#include <rtt/Property.hpp>
#include <rtt/TaskContext.hpp>

#include "orocosservice.hpp"

/**
 * @brief Orocos Property abstract class.
 * Create property of specified type and add it to TaskContext. 
 **/
template <typename T> class OrocosProperty : public OrocosService, public OrocosPropertyInterface
{
	protected:
		RTT::Property< T > * prop;

	public:
		OrocosProperty(RTT::TaskContext * _tc, const std::string& _name, const std::string& _desc) 
			: OrocosService(_tc, _name, _desc)
		{
			if ( tc->properties()->getProperty( name ) == 0 ) {
				//create new Port and add it to TaskContext
				prop = new RTT::Property< T >(name); 
				prop->doc(desc);
				cleanup = true; 
				tc->properties()->addProperty( *prop );
			} 
			else {
				//get pointer 
				prop = tc->properties()->getPropertyType< T >(name);
				cleanup = false;
				if ( !prop ) {
					std::string err_msg;
					err_msg = "Property '" + name + "' has wrong type in TaskContext.";

					log(Error) << err_msg.c_str() << endlog();
					throw OrocosError(err_msg.c_str());
				}
			}
		}

		~OrocosProperty()
		{
			if (cleanup) {
				tc->provides()->removeProperty(*prop);
				delete prop;
			}
		}
};

/**
 * @brief Scalar numerical Orocos Property.
 * Envelop clss for Prperty of scalar numerical types.
 **/
template <typename T> class OrocosScalarProperty : public OrocosProperty< T >
{
	public:
		OrocosScalarProperty(RTT::TaskContext * _tc, const std::string& _name, const std::string& _desc) 
			: OrocosProperty< T >(_tc, _name, _desc)
		{ }

		int getDimention() { return 1; }

		void get(real_T * s_value)
		{
			*s_value = this->prop->get();
		}

		void set(const real_T * s_value)
		{
			*(this->prop) = *s_value;
		}
};

/**
 * @brief Vector numerical Orocos Property.
 * Envelop class for Property of vector numerical types.
 **/
template <typename T> class OrocosVectorProperty : public OrocosProperty< std::vector<T> >
{
	protected:
		std::vector<T> val;
		unsigned int expected_width;

	public:
		OrocosVectorProperty(RTT::TaskContext * _tc, const std::string& _name, const std::string& _desc, unsigned int _expected_width) 
			: OrocosProperty< std::vector<T> >(_tc, _name, _desc), val(_expected_width, 0)
		{
			expected_width = _expected_width;
		}

		int getDimention() { return expected_width; }

		void get(real_T * s_value)
		{
			val = this->prop->get();
			if (val.size() != expected_width) {
				throw OrocosError("Property `" + this->name + "': sample of incorrect width received.");
			}
			for(unsigned int i = 0; i < val.size(); i++) s_value[i] = val[i]; 
		}

		void set(const real_T * s_value)
		{
			for(unsigned int i = 0; i < val.size(); i++) val[i] = s_value[i]; 
			this->prop->set() = val;
		}
};

#endif  /*PROPERTYTYPES_HPP*/
