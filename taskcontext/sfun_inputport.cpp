/*
 * sfuntmpl_basic.c: Basic 'C' template for a level 2 S-function.
 *
 *  -------------------------------------------------------------------------
 *  | See matlabroot/simulink/src/sfuntmpl_doc.c for a more detailed template |
 *  -------------------------------------------------------------------------
 *
 * Copyright 1990-2002 The MathWorks, Inc.
 * $Revision: 1.27 $
 */


/*
 * You must specify the S_FUNCTION_NAME as the name of your S-function
 * (i.e. replace sfuntmpl_basic with the name of your S-function).
 */

#define S_FUNCTION_NAME  sfun_inputport
#define S_FUNCTION_LEVEL 2

/*
 * Need to include simstruc.h for the definition of the SimStruct and
 * its associated macro definitions.
 */
#ifndef MATLAB_MEX_FILE
extern "C"
{
#endif
#include "simstruc.h"
#ifndef MATLAB_MEX_FILE
}
#endif
#ifndef MATLAB_MEX_FILE
#include "sfun_taskcontext.hpp"
#endif

#include "orocoshelperfunc.hpp"

/* Error handling
 * --------------
 *
 * You should use the following technique to report errors encountered within
 * an S-function:
 *
 *       ssSetErrorStatus(S,"Error encountered due to ...");
 *       return;
 *
 * Note that the 2nd argument to ssSetErrorStatus must be persistent memory.
 * It cannot be a local variable. For example the following will cause
 * unpredictable errors:
 *
 *      mdlOutputs()
 *      {
 *         char msg[256];         {ILLEGAL: to fix use "static char msg[256];"}
 *         sprintf(msg,"Error due to %s", string);
 *         ssSetErrorStatus(S,msg);
 *         return;
 *      }
 *
 * See matlabroot/simulink/src/sfuntmpl_doc.c for more details.
 */

#define DESC_IDX  0
#define DESC_PARAM(S) ssGetSFcnParam(S,DESC_IDX)

#define TYPE_IDX   1
#define TYPE_PARAM(S) ssGetSFcnParam(S,TYPE_IDX) 

#define WIDTH_IDX  2
#define WIDTH_PARAM(S) ssGetSFcnParam(S,WIDTH_IDX)

#define INIT_VAL_IDX  3
#define INIT_VAL_PARAM(S) ssGetSFcnParam(S,INIT_VAL_IDX)

#define NEWDATAOUT_FLAG_IDX  4
#define NEWDATAOUT_FLAG_PARAM(S) ssGetSFcnParam(S,NEWDATAOUT_FLAG_IDX)

#define EVENTPORT_FLAG_IDX  5
#define EVENTPORT_FLAG_PARAM(S) ssGetSFcnParam(S,EVENTPORT_FLAG_IDX)

#ifndef MATLAB_MEX_FILE
#include "inputporttypes.hpp"
#endif

/*====================*
 * S-function methods *
 *====================*/

#define MDL_CHECK_PARAMETERS
#if defined(MDL_CHECK_PARAMETERS) && defined(MATLAB_MEX_FILE)
  /* Function: mdlCheckParameters =============================================
   * Abstract:
   *    Validate our parameters to verify they are okay.
   */
static void mdlCheckParameters(SimStruct *S)
{
	if (!mxIsChar(DESC_PARAM(S))) {
		ssSetErrorStatus(S,"Please provide a description as first parameter");
		return;
	}
	if ( !mxIsChar(TYPE_PARAM(S)) ) {
		ssSetErrorStatus(S,"Second parameter to S-function must be a type name.");
		return;
	}
	if (!mxIsNumeric(WIDTH_PARAM(S)) || mxGetNumberOfElements(WIDTH_PARAM(S)) != 1 || mxGetScalar(WIDTH_PARAM(S)) < 1.0) {
		ssSetErrorStatus(S,"Vector size must have positive scalar integer value.");
		return;
	}
	if (!mxIsNumeric(INIT_VAL_PARAM(S))) {
		ssSetErrorStatus(S,"Initial value must have numeric type.");
		return;
	}
	if (!mxIsNumeric(NEWDATAOUT_FLAG_PARAM(S)) || mxGetNumberOfElements(NEWDATAOUT_FLAG_PARAM(S)) != 1) {
		ssSetErrorStatus(S,"NewData flag must be logical scalar.");
		return;
	}
	if (!mxIsNumeric(EVENTPORT_FLAG_PARAM(S)) || mxGetNumberOfElements(EVENTPORT_FLAG_PARAM(S)) != 1) {
		ssSetErrorStatus(S,"EventPort flag must be logical scalar.");
		return;
	}
}
#endif /* MDL_CHECK_PARAMETERS */

/* Function: mdlInitializeSizes ===============================================
 * Abstract:
 *    The sizes information is used by Simulink to determine the S-function
 *    block's characteristics (number of inputs, outputs, states, etc.).
 */
static void mdlInitializeSizes(SimStruct *S)
{
     /* See sfuntmpl_doc.c for more details on the macros below */
    ssSetNumSFcnParams(S, 6);  /* Number of expected parameters */
#if defined(MATLAB_MEX_FILE)
	if (ssGetNumSFcnParams(S) == ssGetSFcnParamsCount(S)) {
		mdlCheckParameters(S);
		if (ssGetErrorStatus(S) != NULL) {
			return;
		}
	} else {
        /* Return if number of expected != number of actual parameters */
        ssSetErrorStatus(S,"InputPort: Expected 6 parameters.");
		return;
	}
#endif
	//Calculate port width
	//TODO Fix this: Unable to create OrocosPort here. Due to unknown reasons RTW::currentTC = 0.
	int port_width;
	char type[255];
	mxGetString(TYPE_PARAM(S), type, 255);
	port_width = OrocosTypeToWidth(type, mxGetScalar(WIDTH_PARAM(S)));
	if (port_width < 0) {
		ssSetErrorStatus(S, "InputPort: InitialiseSizes: port has unknown type.");
		return;
	}
	//Check INIT_VAL dims.
#if defined(MATLAB_MEX_FILE)
	if (mxGetNumberOfElements( INIT_VAL_PARAM(S) ) != port_width) {
        ssSetErrorStatus(S,"InputPort: Dimentions of port and initial value mismatch.");
		return;
	}
#endif

    ssSetSFcnParamTunable(S,DESC_IDX,false); /* Not tunable */
    ssSetSFcnParamTunable(S,TYPE_IDX,false);   /* Not tunable */
    ssSetSFcnParamTunable(S,WIDTH_IDX,false);   /* Not tunable */
    ssSetSFcnParamTunable(S,INIT_VAL_IDX,false);   /* Not tunable */
    ssSetSFcnParamTunable(S,EVENTPORT_FLAG_IDX,false);   /* Not tunable */
    ssSetSFcnParamTunable(S,NEWDATAOUT_FLAG_IDX,false);   /* Not tunable */

    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S,0)) return;

	if (! mxGetScalar(NEWDATAOUT_FLAG_PARAM(S))) {
		// only data output port
		if (!ssSetNumOutputPorts(S,1)) return;
		ssSetOutputPortWidth(S, 0, port_width);
	}
	else {
		// data and NewData flag output port
		if (!ssSetNumOutputPorts(S,2)) return;
		ssSetOutputPortWidth(S, 0, port_width);
		ssSetOutputPortWidth(S, 1, 1);
		ssSetOutputPortDataType(S, 1, SS_BOOLEAN);
	}

    ssSetNumSampleTimes(S, 1);

    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);

    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_CALL_TERMINATE_ON_EXIT);

#ifndef MATLAB_MEX_FILE
	log(Debug) << "MdlInitializeSizes executed." << endlog();
#endif
}


/* Function: mdlInitializeSampleTimes =========================================
 * Abstract:
 *    This function is used to specify the sample time(s) for your
 *    S-function. You must register the same number of sample times as
 *    specified in ssSetNumSampleTimes.
 */
static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, CONTINUOUS_SAMPLE_TIME);
    ssSetOffsetTime(S, 0, FIXED_IN_MINOR_STEP_OFFSET);
}

#undef MDL_INITIALIZE_CONDITIONS   /* Change to #undef to remove function */
#if defined(MDL_INITIALIZE_CONDITIONS)
  /* Function: mdlInitializeConditions ========================================
   * Abstract:
   *    In this function, you should initialize the continuous and discrete
   *    states for your S-function block.  The initial states are placed
   *    in the state vector, ssGetContStates(S) or ssGetRealDiscStates(S).
   *    You can also perform any other initialization activities that your
   *    S-function may require. Note, this routine will be called at the
   *    start of simulation and if it is present in an enabled subsystem
   *    configured to reset states, it will be call when the enabled subsystem
   *    restarts execution to reset the states.
   */
  static void mdlInitializeConditions(SimStruct *S)
  {
  }
#endif /* MDL_INITIALIZE_CONDITIONS */



#define MDL_START  /* Change to #undef to remove function */
#if defined(MDL_START) 
  /* Function: mdlStart =======================================================
   * Abstract:
   *    This function is called once at start of model execution. If you
   *    have states that should be initialized once, this is the place
   *    to do it.
   */
static void mdlStart(SimStruct *S)
{
#ifndef MATLAB_MEX_FILE
	char data[255];
	// name
	std::string name = ssGetModelName(S);
	// desc
	mxGetString(DESC_PARAM(S), data, 255);
	std::string desc = data;
	// type
	mxGetString(TYPE_PARAM(S), data, 255);
	std::string type = data;
	// EventPort flag
	bool eventport_flag = mxGetScalar(EVENTPORT_FLAG_PARAM(S));

	// create port structure
	OrocosInputPortInterface * port;
	try {
		if (type == "double") {
			port = new OrocosScalarInputPort< double >(RTW::currentTC, name, desc, eventport_flag);
		}
		else if (type == "int") {
			port = new OrocosScalarInputPort< int >(RTW::currentTC, name, desc, eventport_flag);
		}
		else if (type == "vector<int>") {
			port = new OrocosVectorInputPort< int >(RTW::currentTC, name, desc, mxGetScalar(WIDTH_PARAM(S)), eventport_flag);
		}
		else if (type == "vector<double>") {
			port = new OrocosVectorInputPort< double >(RTW::currentTC, name, desc, mxGetScalar(WIDTH_PARAM(S)), eventport_flag);
		}
		else {
			ssSetErrorStatus(S, "InputPort: port has unknown type.");
			return;
		}
	}
	catch (OrocosError& error) {
		ssSetErrorStatus(S, error.getMessage().c_str());
		log(Error) << error.getMessage() << endlog();
		return;
	}
	if ( port->getDimention() != ssGetOutputPortWidth(S,0) ) {
		std::string error_msg =  "InputPort: Width of the OROCOS port `" + name + "' is not equal to MATLAB port width.";
		log(Error) << error_msg << endlog();
		ssSetErrorStatus(S, error_msg.c_str());
		return;
	}

	ssSetUserData(S, (void*) port );
	log(Debug) << "MdlStart executed." << endlog();
#endif
}
#endif /*  MDL_START */


/* Function: mdlOutputs =======================================================
 * Abstract:
 *    In this function, you compute the outputs of your S-function
 *    block. Generally outputs are placed in the output vector, ssGetY(S).
 */
static void mdlOutputs(SimStruct *S, int_T tid)
{
    real_T *data     = ssGetOutputPortRealSignal(S,0);

    UNUSED_ARG(tid); /* not used in single tasking mode */

#ifndef MATLAB_MEX_FILE
      OrocosInputPortInterface* port = static_cast<OrocosInputPortInterface *>(ssGetUserData(S));
	  RTT::FlowStatus status = port->read(data);
      if (status == NoData) {
		  // NoData received: set initial value
		  int_T  ndata = ssGetOutputPortWidth(S,0);
		  for(int i = 0; i < ndata; i++) {
			  data[i] = mxGetPr( INIT_VAL_PARAM(S) )[i];
		  }
	  }
	  if (mxGetScalar(NEWDATAOUT_FLAG_PARAM(S))) {
		  boolean_T *newdata = (boolean_T *) ssGetOutputPortSignal(S,1);
		  *newdata = status == NewData;
	  }
#else
	  // Return initial value if running in simulation.
	  int_T  ndata = ssGetOutputPortWidth(S,0);
	  for(int i = 0; i < ndata; i++) {
		  data[i] = mxGetPr( INIT_VAL_PARAM(S) )[i];
	  }
	  if (mxGetScalar(NEWDATAOUT_FLAG_PARAM(S))) {
		  boolean_T *newdata = (boolean_T *) ssGetOutputPortSignal(S,1);
		  *newdata = false;
	  }
#endif
}


#undef MDL_UPDATE  /* Change to #undef to remove function */
#if defined(MDL_UPDATE)
  /* Function: mdlUpdate ======================================================
   * Abstract:
   *    This function is called once for every major integration time step.
   *    Discrete states are typically updated here, but this function is useful
   *    for performing any tasks that should only take place once per
   *    integration step.
   */
  static void mdlUpdate(SimStruct *S, int_T tid)
  {
  }
#endif /* MDL_UPDATE */



#undef MDL_DERIVATIVES  /* Change to #undef to remove function */
#if defined(MDL_DERIVATIVES)
  /* Function: mdlDerivatives =================================================
   * Abstract:
   *    In this function, you compute the S-function block's derivatives.
   *    The derivatives are placed in the derivative vector, ssGetdX(S).
   */
  static void mdlDerivatives(SimStruct *S)
  {
  }
#endif /* MDL_DERIVATIVES */



/* Function: mdlTerminate =====================================================
 * Abstract:
 *    In this function, you should perform any actions that are necessary
 *    at the termination of a simulation.  For example, if memory was
 *    allocated in mdlStart, this is the place to free it.
 */
static void mdlTerminate(SimStruct *S)
{
#ifndef MATLAB_MEX_FILE
	OrocosInputPortInterface* port = static_cast<OrocosInputPortInterface *>(ssGetUserData(S));
	delete port;
	ssSetUserData(S, 0 );
	log(Debug) << "MdlTerminate terminate." << endlog();
#endif
}

#undef MDL_RTW  /* Change to #undef to remove function */
#if defined(MDL_RTW) && defined(MATLAB_MEX_FILE)
  /* Function: mdlRTW =========================================================
   * Abstract:
   *
   *    This function is called when the Real-Time Workshop is generating
   *    the model.rtw file. In this method, you can call the following
   *    functions which add fields to the model.rtw file.
   *
   *       if (!ssWriteRTWWorkVect(S, vectName, nNames,
   *
   *                            name, size,   (must have nNames of these pairs)
   *                                 :
   *                           ) ) {
   *           return;  (error reporting will be handled by SL)
   *       }
   *       Notes:
   *         a) vectName must be either "RWork", "IWork" or "PWork"
   *         b) nNames is an int_T (integer), name is a const char_T* (const
   *            char pointer) and size is int_T, and there must be nNames number
   *            of [name, size] pairs passed to the function.
   *         b) intSize1+intSize2+ ... +intSizeN = ssGetNum<vectName>(S)
   *            Recall that you would have to set ssSetNum<vectName>(S)
   *            in one of the initialization functions (mdlInitializeSizes
   *            or mdlSetWorkVectorWidths).
   *
   *       See simulink/include/simulink.c for the definition (implementation)
   *       of this function, and ... no example yet :(
   *
   */
  static void mdlRTW(SimStruct *S)
  {
  }
#endif /* MDL_RTW */


/*======================================================*
 * See sfuntmpl_doc.c for the optional S-function methods *
 *======================================================*/

/*=============================*
 * Required S-function trailer *
 *=============================*/

#ifndef MATLAB_MEX_FILE
extern "C"
{
#endif
#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
#ifndef MATLAB_MEX_FILE
}
#endif

