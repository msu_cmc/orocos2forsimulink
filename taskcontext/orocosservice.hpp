#ifndef  OROCOSSERVICE_HPP
#define  OROCOSSERVICE_HPP

class OrocosError {
	protected:
		std::string message;
	public:
		OrocosError(const std::string& _message) : message(_message) {}
		OrocosError(const char * _message) : message(_message) {}

		const std::string& getMessage() { return message; }
};

/**
 * @brief Orocos Service abstraction.
 * Orocos Service abstraction: each Service has name, description and assosiated TaskContext.
 **/
class OrocosService
{
	protected:
		RTT::TaskContext * tc;
		std::string name;
		std::string desc;

		bool cleanup;
	public:
		OrocosService(RTT::TaskContext * _tc, const std::string& _name, const std::string& _desc) : 
			tc(_tc), name(_name), desc(_desc), cleanup(false) 
		{
			if (tc == 0) throw OrocosError("Service: Wrong TaskContext pointer.");
		}
};

/**
 * @brief Orocos InputPort Interface for MATLAB.
 * Orocos InputPort Interface for MATLAB.
 **/
class OrocosInputPortInterface
{
	public: 
		virtual int getDimention() = 0;
		virtual RTT::FlowStatus read(real_T * s_out_port) = 0; /** Return false if flow status is NoData. */
		virtual ~OrocosInputPortInterface() {}
};

/**
 * @brief Orocos OutputPort Interface for MATLAB.
 * Orocos OutputPort Interface for MATLAB.
 **/
class OrocosOutputPortInterface
{
	public: 
		virtual int getDimention() = 0;
		virtual void write(const real_T * s_in_port) = 0;
		virtual ~OrocosOutputPortInterface() {}
};


/**
 * @brief Orocos Property Interface for MATLAB.
 * Orocos Property Interface for MATLAB.
 **/
class OrocosPropertyInterface
{
	public: 
		virtual int getDimention() = 0;
		virtual void get(real_T * value) = 0;
		virtual void set(const real_T * value) = 0;
		virtual ~OrocosPropertyInterface() {}
};

#endif  /*OROCOSSERVICE_HPP*/
