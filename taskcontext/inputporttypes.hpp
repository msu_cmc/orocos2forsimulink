#ifndef  INPUTPORTTYPES_HPP
#define  INPUTPORTTYPES_HPP

#include <string>
#include <rtt/InputPort.hpp>
#include <rtt/TaskContext.hpp>
#include <rtt/Logger.hpp>

#include "orocosservice.hpp"

/**
 * @brief Orocos InputPort abstract class.
 * Create port of specified type and add it to TaskContext. 
 **/
template <typename T> class OrocosInputPort : public OrocosService, public OrocosInputPortInterface
{
	protected:
		RTT::InputPort< T > * port;

	public:
		OrocosInputPort(RTT::TaskContext * _tc, const std::string& _name, const std::string& _desc, bool _evnt_flag) : 
			OrocosService(_tc, _name, _desc) 
		{
			if ( tc->ports()->getPort( name ) == 0 ) {
				//create new Port and add it to TaskContext
				port = new RTT::InputPort< T >(name);
				port->doc(desc);
				cleanup = true; 
				if (_evnt_flag) {
					tc->ports()->addEventPort( *port );
				}
				else {
					tc->ports()->addPort( *port );
				}
			} 
			else {
				//get pointer 
				port = tc->ports()->getPortType< RTT::InputPort< T > >(name);
				cleanup = false;
				if ( !port ) {
					std::string err_msg;
					err_msg = "InputPort " + name + " has wrong type in TaskContext.";
					throw OrocosError(err_msg.c_str());
				}
				// check if port is an EventPort
				if (_evnt_flag) {
					//TODO Alternative algorithm witout 'getEventPorts' call. It seems it was removed from OROCOS API.
					/*bool port_found = false;
					for(RTT::DataFlowInterface::Ports::const_iterator p = tc->ports()->getEventPorts().begin(); p != tc->ports()->getEventPorts().end(); p++) {
						if (p->getName() == name) { 
							port_found = true;
							break;
						}
					}
					if (!port_found) {
						std::string err_msg;
						err_msg = "Event InputPort " + name + " had already been registered as common port. Make sure that all blocks with same name has common type.";
						throw OrocosError(err_msg.c_str());
					}*/
				}
			}
		}

		~OrocosInputPort()
		{
			if (cleanup) {
				tc->ports()->removePort(name);
				delete port;
			}
		}
};


/**
 * @brief InputPort envelop for scalar numerical types.
 * InputPort envelop for scalar numerical types.
 **/
template <typename T> class OrocosScalarInputPort : public OrocosInputPort< T >
{
	public:
		OrocosScalarInputPort(RTT::TaskContext * _tc, const std::string& _name, const std::string& _desc, bool _evnt_flag) 
			: OrocosInputPort<T>(_tc, _name, _desc, _evnt_flag)
		{}

		int getDimention() { return 1; }

		RTT::FlowStatus read(real_T * s_out_port)
		{
			T data;
			RTT::FlowStatus status;

			status = this->port->read( data );
			if (NoData != status) {
				*s_out_port = data;
			}
			return status;
		}
};

/**
 * @brief InputPort envelop for vector numerical types.
 * InputPort envelop for vector numerical types.
 **/
template <typename T> class OrocosVectorInputPort : public OrocosInputPort< std::vector<T> >
{
	protected:
		std::vector<T> val;
		unsigned int expected_width;

	public:
		OrocosVectorInputPort(RTT::TaskContext * _tc, const std::string& _name, const std::string& _desc, unsigned int _expected_width, bool _evnt_flag)
			: OrocosInputPort< std::vector<T> >(_tc, _name, _desc, _evnt_flag), 
			val(_expected_width, 0)
		{
			//port->getDataSample(val);
			expected_width = _expected_width;
		}

		int getDimention() { return expected_width; }

		RTT::FlowStatus read(real_T * s_out_port)
		{
			RTT::FlowStatus status;

			status = this->port->read( val );
			if (NoData != status) {
				if (val.size() != expected_width) {
					throw OrocosError("InputPort `" + this->name + "': sample of incorrect width received.");
				}
				for(unsigned int i = 0; i < val.size(); i++) s_out_port[i] = val[i]; 
			}
			return status;
		}
};

#endif  /*INPUTPORTTYPES_HPP*/
