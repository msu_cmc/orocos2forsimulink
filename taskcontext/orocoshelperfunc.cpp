#include <cstring>

int OrocosTypeToWidth(const char * type, int dim)
{
	if (strcmp(type, "double") == 0) return 1;
	else if (strcmp(type, "int") == 0) return 1;
	else if (strcmp(type, "vector<int>")) return dim;
	else if (strcmp(type, "vector<double>")) return dim;
	else return -1;
}
