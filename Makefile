MATLAB_ROOT=/opt/MATLAB/R2011a

FILES=install.m LICENSE LICENSE.txt orocos.tlc orocos.tmf README.Linux README.txt RTWComponent.cpp RTWComponent.hpp setpath.m taskcontext

all:
	cp -ru $(FILES) $(MATLAB_ROOT)/rtw/c/orocos
