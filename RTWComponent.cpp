#include <rtt/Component.hpp>
#include <rtt/TaskContext.hpp>

#include "RTWComponent.hpp"

const char *RT_MEMORY_ALLOCATION_ERROR = "RTWComponent: memory allocation error.";

namespace RTW
{
    RTT::TaskContext* currentTC;
}

ORO_CREATE_COMPONENT(RTW::RTW_COMPONENT)

/***********************************************
 * RTW_COMPONENT class implementation          *
 ***********************************************/

namespace RTW
{
	RTW_COMPONENT::RTW_COMPONENT(const std::string& name)
		: BASECLASS(name, PreOperational),
		rtM(0), 
		FinalTime(RUN_FOREVER)
	{
		log().in( RTW_xstr(RTW_COMPONENT) );
		// We need to create ports and properties asap.
		this->setup();
	}

	RTW_COMPONENT::~RTW_COMPONENT()
	{
		if (this->getTaskState() == Running)
			this->stop();
		if (this->getTaskState() == Stopped)
			this->cleanup();
	}

	bool RTW_COMPONENT::setup()
	{
		const char *status;
		// initialize and return model.

		rt_InitInfAndNaN(sizeof(real_T));

		rtM = ::MODEL();
		if (rtM == NULL) {
			log(Fatal) << "Memory allocation error during target registration" <<endlog();
			return false;
		}
		if (rtmGetErrorStatus(rtM) != NULL) {
			log(Fatal) << "Error during target registration: " << rtmGetErrorStatus(rtM) <<endlog();
			return false;
		}

		if (FinalTime > 0.0 || FinalTime == RUN_FOREVER) {
			rtmSetTFinal(rtM, (real_T)FinalTime);
		}

#if !defined(RT_MALLOC)
		MdlInitializeSizes();
		MdlInitializeSampleTimes();
#else
		rtmiInitializeSizes(rtmGetRTWRTModelMethodsInfo(rtM));
		rtmiInitializeSampleTimes(rtmGetRTWRTModelMethodsInfo(rtM));
#endif

		log(Info) << "Simulink Target info" << endlog();;
		log(Info) << "====================" << endlog();;
		log(Info) << "  Model name             : " << RTW_xstr(MODEL) << endlog();
		log(Info) << "  Model base rate        : " << rtmGetStepSize(rtM) << endlog();
		log(Info) << "  Number of sample times : " << rtmGetNumSampleTimes(rtM) << endlog();
		for (int j = 0; j < rtmGetNumSampleTimes(rtM); j++) {
			log(Info) << "  Sample time "<< j <<"          : " << rtmGetSampleTimePtr(rtM)[j] << endlog();
		}

		status = rt_SimInitTimingEngine(rtmGetNumSampleTimes(rtM), rtmGetStepSize(rtM), rtmGetSampleTimePtr(rtM), rtmGetOffsetTimePtr(rtM), rtmGetSampleHitPtr(rtM), rtmGetSampleTimeTaskIDPtr(rtM), rtmGetTStart(rtM), &rtmGetSimTimeStep(rtM), &rtmGetTimingData(rtM));
		if (status != NULL) {
			log(Fatal) << "Failed to initialize target sample time engine:" << status <<endlog();
			return false;
		}
#ifdef RT_MALLOC
		rt_ODECreateIntegrationData(rtmGetRTWSolverInfo(rtM));
# if NCSTATES > 0
		if(rtmGetErrorStatus(rtM) != NULL) {
			(void)fprintf(stderr, "Error creating integration data.\n");
			this->cleanup();
		}
# endif
#else
		rt_CreateIntegrationData(rtM);
#endif

		// If we don't do this, we get a segfault in MdlStart().
		const char_T *errmsg = rt_StartDataLogging(rtmGetRTWLogInfo(rtM),
				rtmGetTFinal(rtM),
				rtmGetStepSize(rtM),
				&rtmGetErrorStatus(rtM));
		if ( errmsg != NULL) {
			log(Fatal) << "Error starting data logging: " << errmsg << endlog();
			return false;
		}

		// yeah, it ain't thread-safe... The only alternative is to 'steal' a field
		// from the rtM struct and store 'this' inthere, hoping RTW will never overwrite it.
		assert( RTW::currentTC == 0 );
		RTW::currentTC = this;
		// On this point, the blocks are connected and
		// most data is initialised. In start() you can detect
		// connected ports etc and have the latest chance to allocate memory.
		// In orocos, in start() we create the ports, properties etc.
#if !defined(RT_MALLOC)
		MdlStart();
#else
		rtmiStart(rtmGetRTWRTModelMethodsInfo(rtM));
#endif
		if (rtmGetErrorStatus(rtM) != NULL) {
			return false;
		}

		RTW::currentTC = 0;

		status = rtmGetErrorStatus(rtM);
		if ( status != NULL) {
			log(Fatal) << "Failed in target initialization:" << status << endlog();
			// cleanup MdlStart() left-overs:
			this->cleanupHook();
			return false;
		}

		return true;
	}

	bool RTW_COMPONENT::configureHook()
	{
		Logger::In in(RTW_xstr(RTW_COMPONENT));

		// detect re-configuration
		if ( this->getTaskState() == Stopped )
			this->cleanupHook(); // force deletion of old model data

		log(Info) << "Configuring " << this->getName() <<endlog();

		// Give base class a chance to pre-configure
		if ( BASECLASS::configureHook() == false){
			log(Error) << "Baseclass failed to configure" << endlog();
			return false;
		}

		// Since cleanupHook can reset the rtM pointer to zero, we have to rerun
		// setup to make sure rtM points to something real before continuing
		if (this->setup() == false){
			log(Error) << "Setup returned false."<<endlog();
			return false;
		}

		// If the user set a baserate, use that one and re-set all smaller sample times.
		// otherwise, create a matching periodic activity.
		double baserate = this->getActivity()->getPeriod();
		if ( baserate != rtmGetStepSize(rtM) ) {
			if ( baserate == 0.0 ) {
				//TODO Check if Event Ports presents.
				//log(Error) << "Simulink models must be executed at Periodic frequencies, but you've set a non periodic."<<endlog();
				//return false;
				log(Warning) << "Simulink model will be executed with non-periodic Activity." << endlog();
				log(Warning) << "Make sure the model contains Event Port and new data is written to it on base rate " << rtmGetStepSize(rtM) << " s" << endlog();
				//log(Warning) << "If your model contains event and common Input Ports with same names this may cause unpreidictable behavour." << RTT::endlog();
			} else {
				log(Warning) << "Orocos Activity Period = " << baserate << ", Simulink Fixed step Size = " << rtmGetStepSize(rtM) << " -> Forcing base rate to   : " << baserate << endlog();
				rtmSetStepSize( rtM, baserate );
				rtsiSetFixedStepSize( rtM->solverInfo, baserate );
				for (int j = 0; j < rtmGetNumSampleTimes(rtM); j++) {
					if ( rtmGetSampleTimePtr(rtM)[j] != 0.0 
							&& rtmGetSampleTimePtr(rtM)[j] < baserate ) {
						log(Warning) << "  Forcing sample time "<< j <<" to: " << baserate << endlog();
						rtmGetSampleTimePtr(rtM)[j] = baserate;
					}
				}
			}
		}
		return true;
	}


	void RTW_COMPONENT::cleanupHook()
	{
		assert( RTW::currentTC == 0 );
		RTW::currentTC = this;
		if ( rtM ) {
			rt_StopDataLogging(MATFILE,rtmGetRTWLogInfo(rtM));
			/* timing data */
			rt_SimDestroyTimingEngine(rtmGetTimingData(rtM));
#if NCSTATES > 0
			/* integration data */
			rt_ODEDestroyIntegrationData(rtmGetRTWSolverInfo(rtM));
#endif

#if !defined(RT_MALLOC)
			MdlTerminate();
#else
			rtmiTerminate(rtmGetRTWRTModelMethodsInfo(rtM));
#endif
		}
		RTW::currentTC = 0;
		rtM = 0;

		BASECLASS::cleanupHook();
	}

	void RTW_COMPONENT::updateHook()
	{
		real_T tnext;

#ifndef RT_MALLOC
		// This snippet comes from grt_main.c, official mathworks example.
		// It is the !MULTI_TASKING setup

		/***********************************************
		 * Check and see if error status has been set  *
		 ***********************************************/

		if (rtmGetErrorStatus(rtM) != NULL) {
			this->stop();
			return;
		}

#ifdef EXT_MODE
		/*
		 * In a multi-tasking environment, this would be removed from the base rate
		 * and called as a "background" task.
		 */
		rtExtModeOneStep(rtmGetRTWExtModeInfo(rtM),
				rtmGetNumSampleTimes(rtM),
				(boolean_T *)&rtmGetStopRequested(rtM));
#endif

		tnext = rt_SimGetNextSampleHit();
		rtsiSetSolverStopTime(rtmGetRTWSolverInfo(rtM),tnext);

		MdlOutputs(0);

#ifdef EXT_MODE
		rtExtModeSingleTaskUpload(rtM);

		GBLbuf.errmsg = rt_UpdateTXYLogVars(rtmGetRTWLogInfo(rtM),rtmGetTPtr(rtM));
		if (GBLbuf.errmsg != NULL) {
			GBLbuf.stopExecutionFlag = 1;
			return;
		}
#endif

		MdlUpdate(0);
		rt_SimUpdateDiscreteTaskSampleHits(rtmGetNumSampleTimes(rtM),
				rtmGetTimingData(rtM),
				rtmGetSampleHitPtr(rtM),
				rtmGetTPtr(rtM));

		if (rtmGetSampleTime(rtM,0) == CONTINUOUS_SAMPLE_TIME) {
			rt_UpdateContinuousStates(rtM);
		}

#ifdef EXT_MODE
		rtExtModeCheckEndTrigger();
#endif
#else // RT_MALLOC

#ifdef EXT_MODE
		rtExtModeOneStep(rtmGetRTWExtModeInfo(rtM),
				rtmGetNumSampleTimes(rtM),
				(boolean_T *)&rtmGetStopRequested(rtM));
#endif

		tnext = rt_SimGetNextSampleHit(rtmGetTimingData(rtM),
				rtmGetNumSampleTimes(rtM));
		rtsiSetSolverStopTime(rtmGetRTWSolverInfo(rtM),tnext);

		rtmiOutputs(rtmGetRTWRTModelMethodsInfo(rtM),0);

#ifdef EXT_MODE
		rtExtModeSingleTaskUpload(rtM);

		GBLbuf.errmsg = rt_UpdateTXYLogVars(rtmGetRTWLogInfo(rtM),
				rtmGetTPtr(rtM));
		if (GBLbuf.errmsg != NULL) {
			GBLbuf.stopExecutionFlag = 1;
			return;
		}
#endif

		rtmiUpdate(rtmGetRTWRTModelMethodsInfo(rtM),0);

		rt_SimUpdateDiscreteTaskSampleHits(rtmGetNumSampleTimes(rtM),
				rtmGetTimingData(rtM),
				rtmGetSampleHitPtr(rtM),
				rtmGetTPtr(rtM));

		if (rtmGetSampleTime(rtM,0) == CONTINUOUS_SAMPLE_TIME) {
			rt_ODEUpdateContinuousStates(rtmGetRTWSolverInfo(rtM));
		}

		//             GBLbuf.isrOverrun--;

#ifdef EXT_MODE
		rtExtModeCheckEndTrigger();
#endif

#endif
	}

} /* namespase RTW*/
