#ifndef OCL_RTW_COMPONENT_HPP
#define OCL_RTW_COMPONENT_HPP

extern "C"
{
#include "tmwtypes.h"
#include "rtmodel.h"
#include "rt_sim.h"
#include "rt_nonfinite.h"
#include "mdl_info.h"
#include "bio_sig.h"
#include "simstruc.h"
} // extern "C"

#define EXPAND_CONCAT(name1,name2)	name1 ## name2
#define CONCAT(name1,name2)		EXPAND_CONCAT(name1,name2)
#define RT_MODEL			CONCAT(MODEL,_rtModel)

#if TID01EQ == 1
#define FIRST_TID 1
#else
#define FIRST_TID 0
#endif
#ifndef RT
# error "must define RT"
#endif
#ifndef MODEL
# error "must define MODEL"
#endif
#ifndef NUMST
# error "must define number of sample times, NUMST"
#endif
#ifndef NCSTATES
# error "must define NCSTATES"
#endif

#ifndef SAVEFILE
# define MATFILE2(file) #file ".mat"
# define MATFILE1(file) MATFILE2(file)
# define MATFILE MATFILE1(MODEL)
#else
# define MATFILE QUOTE(SAVEFILE)
#endif

#define RUN_FOREVER		-1.0

/**
 * Allow other bases than TaskContext.
 */
#ifndef BASEHEADER
	#define BASEHEADER <rtt/TaskContext.hpp>
	#ifndef BASECLASS
		#define BASECLASS TaskContext
	#endif
#endif

#ifndef RT_MALLOC
	#error "Use the RT_MALLOC flag for building components."
#endif


/*====================*
 * External functions *
 *====================*/
/**
 * extern 'C' Function declarations.
 */
extern "C" {

	extern RT_MODEL *MODEL(void);

#if !defined(RT_MALLOC)
	/**
	 * This is the GRT compatible call interface for non-malloc
	 * targets. The malloc target uses different functions, using
	 * the model as function argument.
	 */
	extern void MdlInitializeSizes(void);
	extern void MdlInitializeSampleTimes(void);
	extern void MdlStart(void);
	extern void MdlOutputs(int_T tid);
	extern void MdlUpdate(int_T tid);
	extern void MdlTerminate(void);
#endif

#ifndef RT_MALLOC
#if NCSTATES > 0
	extern void rt_ODECreateIntegrationData(RTWSolverInfo *si);
	extern void rt_ODEUpdateContinuousStates(RTWSolverInfo *si);

# define rt_CreateIntegrationData(S)			\
	rt_ODECreateIntegrationData(rtmGetRTWSolverInfo(S));
# define rt_UpdateContinuousStates(S)			\
	rt_ODEUpdateContinuousStates(rtmGetRTWSolverInfo(S));
# else
# define rt_CreateIntegrationData(S)					\
	rtsiSetSolverName(rtmGetRTWSolverInfo(S),"FixedStepDiscrete");
# define rt_UpdateContinuousStates(S) /* Do Nothing */
#endif

#else // !RT_MALLOC:

#if NCSTATES > 0
	extern void rt_ODECreateIntegrationData(RTWSolverInfo *si);
	extern void rt_ODEUpdateContinuousStates(RTWSolverInfo *si);
# if defined(RT_MALLOC)
	extern void rt_ODEDestroyIntegrationData(RTWSolverInfo *si);
# endif
#else
# define rt_ODECreateIntegrationData(si)	\
	rtsiSetSolverName(si, "FixedStepDiscrete");
# define rt_ODEUpdateContinuousStates(si)	\
	rtsiSetT(si,rtsiGetSolverStopTime(si))
#endif
#endif // !RT_MALLOC

} // extern "C"

#ifdef EXT_MODE
#  define rtExtModeSingleTaskUpload(S)                          \
{								\
	int stIdx;							\
	rtExtModeUploadCheckTrigger(rtmGetNumSampleTimes(S));	\
	for (stIdx=0; stIdx<NUMST; stIdx++) {			\
		if (rtmIsSampleHit(S, stIdx, 0 /*unused*/)) {		\
			rtExtModeUpload(stIdx,rtmGetTaskTime(S,stIdx));		\
		}								\
	}								\
}
#else
#  define rtExtModeSingleTaskUpload(S) /* Do nothing */
#endif

#define RTW_COMPONENT MODEL
#define RTW_xstr(s) RTW_str(s)
#define RTW_str(s) #s        

#include BASEHEADER
#include <rtt/Attribute.hpp>
#include "taskcontext/sfun_taskcontext.hpp"

namespace RTW
{
	using namespace RTT;

	struct RTWCreationException
	{};

	/**
	 * The Auto-generated class for hosting a MatLab Simulink algorithm.
	 * The \a RTW_COMPONENT classname is actually a macro which expands to
	 * "<modelname>" and is thus unique for each model.
	 */
	class RTW_COMPONENT
		: public BASECLASS
	{
		protected:
			RT_MODEL *rtM;
			float FinalTime;

		public:
			RTW_COMPONENT(const std::string& name);
			virtual ~RTW_COMPONENT();

			bool setup();
			bool configureHook();
			void cleanupHook();
			bool startHook()
			{
				// Check if model rate matches execution frequency.
				//             if ( this->engine()->getActivity() == 0 )
				//                 return false;
				//             if ( this->engine()->getActivity()->getPeriod() != rtmGetStepSize(rtM) )
				//                 return false;
				return  BASECLASS::startHook();
			}

			void updateHook();
	};
}

#endif
