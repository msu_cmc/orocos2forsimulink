function install()

setpath;

disp('Orocos: configuring mex...')
mex -setup

disp('Orocos: compiling blocks...')
cd taskcontext;
mex sfun_outputport.cpp orocoshelperfunc.cpp;
mex sfun_inputport.cpp orocoshelperfunc.cpp;
mex sfun_property.cpp orocoshelperfunc.cpp;
cd ..;

disp('Orocos: install completed.')
